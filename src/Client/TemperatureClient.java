package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Client which sends messages to server. Used in unitTest
 */
public class TemperatureClient {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void startConnecting(String ip, int port) throws IOException {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    /**
     *
     * @param celsius this is the input send to the client; the temperature in celsius
     * @return a double which is the celsius temp converted to fahrenheit temp
     * @throws IOException occurs when out fails
     */
    public double convertCelsiusToFahrenheit(double celsius) throws IOException {
        out.println(celsius);
        double response = Double.parseDouble(in.readLine());
        return response;
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
    }
}
