package Client;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TempClientLauncher {

    private static final Scanner userInput = new Scanner(System.in);
    private double celsius;

    public TempClientLauncher() {
        super();
    }

    private void run() {
        TemperatureClient client = new TemperatureClient();
        try {
            client.startConnecting("127.0.0.1", 5555);
            System.out.println("This server converts a temperature given in celsius, to a temperature in fahrenheit.");
            System.out.println("To stop, enter -273,15 (this is the absolute zero temperature in celsius.)");
            do {
                System.out.print("Give a temperatuer in celsius to convert: ");
                try {
                    celsius = userInput.nextDouble();
                } catch (InputMismatchException inputError) {
                    System.out.println("You need to enter a valid temperature, consisting of numbers and a comma (for example: 15,2)");
                    userInput.next();
                }
                if (celsius == -273.15) {
                    System.out.println("This program was terminated by you.");
                    break;
                }
                try {
                    System.out.println(celsius + " degrees celsius is " + client.convertCelsiusToFahrenheit(celsius) + " degree in fahrenheit.");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            } while (true);
            } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new TempClientLauncher().run();
    }

}
