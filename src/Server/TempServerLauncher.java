package Server;

import java.io.IOException;

public class TempServerLauncher {

    public TempServerLauncher() {
        super();
    }

    /**
     * To run the seperate servers, comment out the server you don't want to use
     * @throws IOException
     */
    private void run() throws IOException {
        //runTemperatureServer();
        runTemperatureEchoServer();
    }

    /**
     * method to run the Server.TemperatureServer
     * @throws IOException
     */
    private void runTemperatureServer() throws IOException {
        TemperatureServer temperatureServer = new TemperatureServer();
        temperatureServer.start(6666);
    }

    /**
     * methot to run the Server.TemperatureEchoServer
     * @throws IOException
     */
    private void runTemperatureEchoServer() throws IOException {
        TemperatureEchoServer temperatureEchoServer = new TemperatureEchoServer();
        temperatureEchoServer.start(5555);
    }

    public static void main(String[] args) throws IOException {
        new TempServerLauncher().run();
    }


}
