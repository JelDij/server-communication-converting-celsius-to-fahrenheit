package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server which accepts multiple requests. Server is stopped once the absolute zero is send.
 */
public class TemperatureEchoServer {

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        // server stops when input is equal to absolute zero: -273.15
        while (true) {
            double input = Double.parseDouble(in.readLine());
            if (input == -273.15) {
                out.println(input);
                break;
            }
            double result = input * 1.8000 + 32;
            out.println(result);
        }
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();
    }
}

