package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server which accepts a single request and converts the double to fahrenheit.
 */
public class TemperatureServer {

    private ServerSocket serverSocket;
    private Socket clientScoket;
    private PrintWriter out;
    private BufferedReader in;

    /**
     * Method starts server and converts input to fahrenheit; it is not checked if input is sent correctly
     * Server closes after one input
     * @param port this is the listen port
     * @throws IOException throws exception if anythong goes wrong with the input/output
     */
    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        clientScoket = serverSocket.accept();
        out = new PrintWriter(clientScoket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientScoket.getInputStream()));
        double celsius = Double.parseDouble(in.readLine());
        double fahrenheit = celsius * 1.8000 + 32.00;
        out.println(fahrenheit);
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientScoket.close();
        serverSocket.close();
    }
}
