import Client.TemperatureClient;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests temperatureServer in 3 different tests by sending a message to the server and checking response.
 */

class TermperatureServerTest {


    @Test
    public void convertCelsiusToFahrenheit_whenServerRespondsCorrectly_thenCorrect_01() throws IOException {
        TemperatureClient client = new TemperatureClient();
        client.startConnecting("127.0.0.1", 6666);
        double response1 = client.convertCelsiusToFahrenheit(30);
        assertEquals(86.0, response1);
    }

    @Test
    public void convertCelsiusToFahrenheit_whenServerRespondsCorrectly_thenCorrect_02() throws IOException {
        TemperatureClient client = new TemperatureClient();
        client.startConnecting("127.0.0.1", 6666);
        double response1 = client.convertCelsiusToFahrenheit(-100);
        assertEquals(-148, response1);
    }

    @Test
    public void convertCelsiusToFahrenheit_whenServerRespondsCorrectly_thenCorrect_03() throws IOException {
        TemperatureClient client = new TemperatureClient();
        client.startConnecting("127.0.0.1", 6666);
        double response1 = client.convertCelsiusToFahrenheit(100);
        assertEquals(212, response1);
    }



}