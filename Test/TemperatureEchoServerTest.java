import Client.TemperatureClient;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the TemperatureEcho server by using the temperatureClient to send messages to said server
 *
 */

class TemperatureEchoServerTest {

    @Test
    public void convertCelsiusToFahrenheit_whenServerRespondsCorrectly_thenCorrect_01() throws IOException {
        TemperatureClient client = new TemperatureClient();
        client.startConnecting("127.0.0.1", 5555);
        double response1 = client.convertCelsiusToFahrenheit(30);
        double response2 = client.convertCelsiusToFahrenheit(100);
        double response3 = client.convertCelsiusToFahrenheit(-100);
        double response4 = client.convertCelsiusToFahrenheit(-273.15);
        assertEquals(86.0, response1);
        assertEquals(212, response2);
        assertEquals(-148, response3);
        assertEquals(-273.15, response4);
    }

}